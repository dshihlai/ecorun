#!/bin/python
import os
import json
import subprocess
import sys
import platform


class ApplicationManager(object):
    '''ApplicationManager Class. Basically, it discovers applications and
    various executables. Also launches a software.'''
    def __init__(self):
        self.launcher_path = os.path.split(os.path.dirname(__file__))[0]
        self.apps_path = os.getenv(
            'ECORUN_APPS_FOLDER',
            os.path.join(self.launcher_path, 'apps')
        )
        self.presets = {}
        self.eco_exec = self.get_eco_exec()
        self.discover()

    def is_win(self):
        return 'windows' == platform.system().lower()

    def get_eco_exec(self):
        '''Returns the ecosystem executable path as a string.

        :return: Path to the Ecosystem executable
        :rtype: str
        '''
        eco_root = os.getenv('ECO_ROOT')
        if not eco_root:
            raise ValueError('Ecosystem must be configured.')
        args = [eco_root, 'bin']
        if self.is_win():
            args.append('eco.cmd')
        else:
            args.append('ecosystem.py')
        return os.path.join(*args)

    def get_eco_command(self, preset, args):
        '''Builds depending on the host OS the proper ecosystem command to be
        executed.

        :param application: Name of the application (folder) to be launched.
        :param preset: Name of the preset (json) to be loaded.
        :param args: Extra arguments to be passed to the software.
        :type application: str
        :type preset: str
        :type args: list

        :return: Command to be executed by python
        :rtype: list
        '''
        dependencies, launcher = map(preset.data.get, ['requires', 'launcher'])
        dependencies = ','.join(dependencies)
        win_cmd = [self.eco_exec, '-t', dependencies, '-r', launcher]
        lin_cmd = [self.eco_exec, dependencies]
        if self.is_win():
            cmd = win_cmd
        else:
            cmd = lin_cmd
        cmd += args
        return cmd

    def discover(self):
        '''Looks in the ECORUN_APPS_FOLDER environment variable or (by default)
        in the apps folder in the root directory and populates
        "self.presets" with the presets found.
        '''

        for app_path in self.apps_path.split(os.pathsep):
            for root, dirs, files in os.walk(app_path):
                relative_root = root[len(app_path)+1:]
                jsonfiles = [x for x in files if x.endswith('.json')]
                for jsonfile in jsonfiles:
                    preset = Preset(os.path.join(root, jsonfile))
                    if preset.is_valid():
                        _relative_root = relative_root.split(os.sep)
                        jsonfile = os.path.splitext(jsonfile)[0]
                        preset_name = '/'.join(_relative_root + [jsonfile])
                        preset.name = preset_name
                        self.presets[preset_name] = preset

    def launch(self, preset, args=[], blocking=False):
        '''Launches an ecosystem instance in the system with a built command
        based on a preset and (optional) extra arguments.

        :param preset: Relative path to JSON file.
        :param args: Extra arguments to be passed to the software.
        :type preset: str
        :type args: list
        '''

        if preset not in self.presets:
            raise ValueError('Preset "%s" not found' % preset)
        preset = self.presets.get(preset)

        cmd = self.get_eco_command(preset, args)
        os.environ['ECORUN_PRESET'] = str(preset.name)
        proc = subprocess.Popen(cmd, env=os.environ)
        if blocking:
            proc.wait()
            sys.exit(proc.returncode)

    def parse_args(self):
        '''Converts the arguments into a valid ApplicationManager information.

        :return: Application name, preset name and extra arguments.
        :rtype: list, str
        '''

        args = sys.argv[1:]
        extra = []
        if '--from-env' in args:
            preset = os.getenv('ECORUN_PRESET')
        elif '--list' in args or '-l' in args:
            preset = '--list'
        else:
            preset = args.pop(0)
        extra += args
        return preset, extra


class Preset(object):
    '''From a json file, builds a valid preset.

    :param path: Path to the preset json file.
    :type path: str
    '''
    def __init__(self, path):
        self.path = path
        self.data = {}
        self.setup(path)

    def __repr__(self):
        return '<ecorun.Preset "%s">' % self.name

    def setup(self, path):
        '''Reads the JSON file and populates self.data if valid.
        '''
        with file(path, 'r') as f:
            try:
                data = json.load(f)
            except ValueError:
                msg = 'Json file "%s" is not valid.'
                print msg % path
                return
        if self.preset_is_valid(data):
            self.data = data

    def preset_is_valid(self, preset):
        '''Determines if a preset has the minimum keys to ve valid.

        :param preset: The content of the json file as a dict.
        :type preset: dict

        :return: Wether the preset is valid or not.
        :rtype: bool
        '''
        requires = ['requires', 'launcher']
        return all([x in preset for x in requires])

    def is_valid(self):
        '''Returns if the application is valid by looking at the number of
        valid presets within.

        :return: Wether the application is valid or not.
        :rtype: bool
        '''
        return bool(self.data)


if __name__ == '__main__':
    appmanager = ApplicationManager()
    preset, extra = appmanager.parse_args()
    if preset == '--list':
        for key in appmanager.presets.keys():
            sys.stdout.write(key+'\n')
        sys.exit()
    appmanager.launch(
        preset,
        extra
    )
